package com.jsfondi;

import java.util.HashMap;

public class Fruit implements Comparable<Fruit> {
    String name;
    float price;

    public Fruit(String name, float price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public String toString() {
        return String.format("%s (%s)", this.name, this.price);
    }

    @Override
    public int compareTo(Fruit fruit) {
        return -Float.compare(this.price, fruit.price);
    }

    public int getTotalThatCanBeBought(float cashInHand) {
        return (int) (cashInHand / this.price);
    }

    public boolean isFruitAlreadyBought(HashMap<Fruit, Integer> fruitsBought) {
        return fruitsBought.containsKey(this);
    }
}
