package com.jsfondi;


//apple 59
//banana 32
//coconut 155
//grapefruit 128
//jackfruit 1100
//kiwi 41
//lemon 70
//mango 97
//orange 73
//papaya 254
//pear 37
//pineapple 399
//watermelon 500


import java.util.*;

public class Main {

    private static final float WALLET_CAPACITY = 500;
    private static List<Fruit> fruitsList = Arrays.asList(
            new Fruit("apple", 59),
            new Fruit("banana", 32),
            new Fruit("coconut", 155),
            new Fruit("grapefruit", 128),
            new Fruit("jackfruit", 1100),
            new Fruit("kiwi", 41),
            new Fruit("lemon", 70),
            new Fruit("mango", 97),
            new Fruit("orange", 73),
            new Fruit("papaya", 254),
            new Fruit("pear", 37),
            new Fruit("pineapple", 399),
            new Fruit("watermelon", 500)
    );

    private static float remainingBalance = WALLET_CAPACITY;
    private static HashMap<Fruit, Integer> fruitsBought = new HashMap<>();
    private static Set<String> results = new HashSet<>();

    public static void main(String[] args) {
        Collections.sort(fruitsList);
        compute();
        int resultCount = 1;
        for (String result : results) {
            System.out.println(resultCount++ + ". " + result);
        }
    }

    private static void compute() {

        for (Fruit fruit : fruitsList) {

            if (fruit.isFruitAlreadyBought(fruitsBought)) {
                continue;
            }

            int fruitCount = fruit.getTotalThatCanBeBought(remainingBalance);

            if (fruitCount > 0) {
                for (int i = 0; i <= fruitCount; i++) {
                    cleanCheaperFruitsBought(fruit);
                    fruitsBought.put(fruit, i);
                    remainingBalance = computeRemainingBalance();
                    if (remainingBalance == 0) {
                        results.add(saveResult());
                    }
                    if (remainingBalance > 0) {
                        compute();
                    }
                }
            }
        }
    }

    private static float computeRemainingBalance() {
        float totalCost = 0;
        for (Map.Entry<Fruit, Integer> fruitEntry : fruitsBought.entrySet()) {
            totalCost += fruitEntry.getKey().price * fruitEntry.getValue();
        }
        return WALLET_CAPACITY - totalCost;
    }

    private static void cleanCheaperFruitsBought(Fruit fruit) {
        if (!fruitsBought.isEmpty()) {
            List<Fruit> fruitsToRemove = new ArrayList<>();
            fruitsBought.forEach((f, c) -> {
                if (fruit.price > f.price) {
                    fruitsToRemove.add(f);
                }
            });
            for (Fruit f : fruitsToRemove) {
                fruitsBought.remove(f);
            }
        }
    }

    private static String saveResult() {
        StringBuilder sb = new StringBuilder();
        fruitsBought.forEach((f, c) -> {
            if (c > 0) {
                sb.append(String.format("%s %s(s) ", c, f.name));
            }
        });
        return sb.toString();
    }
}
